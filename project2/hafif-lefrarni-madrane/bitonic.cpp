
using namespace std;

#include <mpi.h>
#include <iostream>
#include <string>
#include <vector>
#include <math.h>
#include <set>

#define TAG_DATA 0
#define TAG_COMP_EXCH 1
#define TAG_MERGE_END 2

/**
 *  \brief Print the content of a list
 *  \param sortedSequence the sorted list from bitonic sort process
 */
void printSequence(const vector<int>& sortedSequence){
	cout<<"Sorted sequence : ";
	for(int element : sortedSequence){
		cout<<element<<" ";
	}
	cout<<endl;
}

/**
 *  \brief Receives and merge all sorted vector result of comparator process
 *  \param sortedSequence Vector where sub-sorted vector of comparator process are merged
 *  \param data Buffer where master put received data from comparator process
 *  \param nb_instance Number of process in the mpi
 *  \param elmntPerProcess Size of the data processed by each comparator process
 *  \param STATUS MPI param for the status of the MPI_Recv call
 */
void mergeSubSortedSequence(vector<int>& sortedSequence, vector<int>& data, 
	const int& nb_instance, const int& elmntPerProcess, MPI_Status& STATUS){
	for(int i = 1; i<nb_instance; i++){
		MPI_Recv(&data[0], elmntPerProcess, MPI_INT, i, TAG_MERGE_END, MPI_COMM_WORLD, &STATUS);
		for(int element : data){
			sortedSequence.push_back(element);
		}
	}
}

/**
 *  \brief Compare and swap method
 *  \param a first number of the comparison
 *  \param b second number of the comparison
 */
void compareHighAndExchange(int& a, int& b){
	if(a > b){
		int temp = a;
		a = b;
		b = temp;
	}
}

int main(int argc, char *argv[]) {
	int processRank, nb_instance;

	MPI_Status STATUS;

	MPI_Init(&argc,&argv);
	MPI_Comm_size(MPI_COMM_WORLD, &nb_instance);
	MPI_Comm_rank(MPI_COMM_WORLD, &processRank);

	int cnodes = nb_instance - 1;
	int n = cnodes * 2;
	int elmntPerProcess = n / cnodes;

	vector<int> data(elmntPerProcess); /* buffer for the data sent between process */
	vector<int> comp(2); /* buffer for the comparison data sent between process on a comparison communication */

	if(n != 16){
		if(processRank == 0){
			cout<<"Error : Number of processor must be (n/2)+1, where n is the size of the bitonic sequence to sort"<<endl;
		}
	}else{
		if(processRank == 0){
		// Master part
			vector<int> bitonicSequence = {14, 16, 15, 11, 9, 8, 7, 5, 4, 2, 1, 3, 6, 10, 12, 13};
			vector<int> sortedSequence; /* vector where the final sorted sequence will be */

			/* Master start by sending to each comparator process the parts of the bitonic sequence they need to sort */
			for(int rank = 1; rank < nb_instance; rank++){
				for(int j = 0; j < elmntPerProcess; j++){
					data[j] = bitonicSequence[((rank-1)*elmntPerProcess)+j];
				}
				MPI_Send(&data[0], elmntPerProcess, MPI_INT, rank, TAG_DATA, MPI_COMM_WORLD);
			}

			/* Master wait to receive the sorted parts from each comparator process before printing the sorted sequence */
			mergeSubSortedSequence(sortedSequence, data, nb_instance, elmntPerProcess, STATUS);
			printSequence(sortedSequence);
		}else{
		// Comparators Process part
		    /* Every comparator start by waiting to receive their parts of the sequence to sort */
			MPI_Recv(&data[0], elmntPerProcess, MPI_INT, 0, TAG_DATA, MPI_COMM_WORLD, &STATUS);

            /* Number of communication step between comparators to sort their parts */
			int numberOfStep = log2(n);
			for(int step = 0; step < numberOfStep-1; step++){
			    /* Finding process who will initiate a comparison communication with their corresponding correspondent of each step */
				set<int> procCompInitiator;
				int groupSize = (nb_instance-1)/pow(2,step); /* Size of groups of process communicating with each other */
				int groupComSplitter = groupSize/2; /* Split a group in two parts : process initiating comparison communication and process receiving it */
				for(int i = 0; i<(nb_instance-1); i=i+groupSize){
					for(int j = i; j<(i+groupComSplitter); j++){
						procCompInitiator.insert(j+1);
					}
				}

				if(procCompInitiator.find(processRank) != procCompInitiator.end()) {
				    /* Processes initiating a comparison communication */
					for(int i = 0; i < data.size(); i++){
						comp[0] = i;
						comp[1] = data[i];
						MPI_Send(&comp[0], 2, MPI_INT, (groupComSplitter+processRank), TAG_COMP_EXCH, MPI_COMM_WORLD);
						MPI_Recv(&comp[0], 2, MPI_INT, (groupComSplitter+processRank), TAG_COMP_EXCH, MPI_COMM_WORLD, &STATUS);
						data[comp[0]] = comp[1];
					}
				}else{
				    /* Processes receiving a comparison communication, only those processes execute the compare swap on value */
					for(int i = 0; i < data.size(); i++){
						MPI_Recv(&comp[0], 2, MPI_INT, (processRank-groupComSplitter), TAG_COMP_EXCH, MPI_COMM_WORLD, &STATUS);
						compareHighAndExchange(comp[1], data[comp[0]]);
						MPI_Send(&comp[0], 2, MPI_INT, (processRank-groupComSplitter), TAG_COMP_EXCH, MPI_COMM_WORLD);
					}
				}
			}

			/* Last compare and swap done by every process comparator, only on their parts */
			compareHighAndExchange(data[0], data[1]);

			/* Each comparator process send the parts he sorted to the Master */
			MPI_Send(&data[0], elmntPerProcess, MPI_INT, 0, TAG_MERGE_END, MPI_COMM_WORLD);
		}
	}
	MPI_Finalize();
	return 0;
}