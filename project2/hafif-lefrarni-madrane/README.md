Project 2 Parallel Bitonic Sort

commands to run the project:

	> module load OpenMPI/2.1.1-GCC-6.4.0-2.28
	> mpiCC bitonic.cpp -o bitonic
	> mpirun -np 9 ./bitonic
