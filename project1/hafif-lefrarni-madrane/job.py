
class Job:
    def __init__(self, instance_number, task_identifier, computation, absolute_deadline, release_time, priority):
        self.instance_number = instance_number
        self.task_identifier = task_identifier
        self.computation = computation
        self.absolute_deadline = absolute_deadline
        self.release_time = release_time
        self.priority = priority
        self.consumed_computation = 0

    def get_instance_number(self):
        return self.instance_number

    def get_task_identifier(self):
        return self.task_identifier

    def get_computation(self):
        return self.computation

    def get_absolute_deadline(self):
        return self.absolute_deadline

    def get_release_time(self):
        return self.release_time

    def get_priority(self):
        return self.priority

    def get_consumed_computation(self):
        return self.consumed_computation

    def increase_consumed_computation(self):
        self.consumed_computation += 1
