import matplotlib.pyplot as plt
import matplotlib.backends.backend_pdf


def plot_simulation(x):
    pdf = matplotlib.backends.backend_pdf.PdfPages("SchedulingVisualOutput.pdf")
    pdf.savefig(plot_system_schedule(x, 1), orientation='landscape')
    pdf.close()
    return 0


def plot_system_schedule(x, nb):
    interval = len(x)
    subplot = 1
    index_insert = 0
    fig = plt.figure()
    ax = fig.add_subplot(111)
    ax.spines['top'].set_color('none')
    ax.spines['bottom'].set_color('none')
    ax.spines['left'].set_color('none')
    ax.spines['right'].set_color('none')
    ax.tick_params(labelcolor='w', top='off', bottom='off', left='off', right='off')
    ax.set_title('Processor ' + str(nb))

    tasks = []
    for var in x:
        if var not in tasks and var != -1:
            tasks.append(var)

    elements = []
    for i in range(len(tasks)):
        elements.append([])

    for idx, val in enumerate(x):
        for i, task in enumerate(tasks):
            if val != -1 and val == task:
                index_insert = i
                break
        if val != -1:
            elements[index_insert].append(idx)

    subplots = []
    for var in elements:
        subplots.append(fig.add_subplot(len(tasks), 1, subplot))
        subplot = subplot + 1

    for idx, val in enumerate(subplots):
        plot_task(elements[idx], tasks[idx], val, interval)

    # customize axis
    for ax in subplots:
        ax.set_ylim(0, 1.2)
        ax.spines['right'].set_visible(False)
        ax.spines['top'].set_visible(False)
        ax.yaxis.set_ticks_position('left')
        ax.xaxis.set_ticks_position('bottom')

    plt.tight_layout()
    return fig


def plot_task(arr, nb, subplot, interval):
    import numpy as np
    cpu_t = []
    cpu_p = []
    cpu_t.append(0)
    cpu_p.append(0)

    for i in range(interval+1):
        if i in arr:
            if i-1 not in arr:
                cpu_t.append(i)
                cpu_p.append(0)
            cpu_t.append(i)
            cpu_p.append(1)
        else:
            if i-1 in arr:
                cpu_t.append(i)
                cpu_p.append(1)

            cpu_t.append(i)
            cpu_p.append(0)

    # plot 1
    subplot.fill_between(cpu_t, cpu_p, 0, color='b', edgecolor='k')
    subplot.set_ylabel(u'\u03c4'+str(nb), fontname='STIXGeneral', size=15,rotation=0, va='center', ha='center', labelpad=20)

    # plot 2
    minor_ticks = np.arange(0, interval+1, 5)
    subplot.get_xlim()
    subplot.set_xticks(minor_ticks, minor=True)
    subplot.axes.get_yaxis().set_ticks([])
    return subplot
