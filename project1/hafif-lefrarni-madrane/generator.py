import random
from task import *


class Generator:
    def __init__(self, nb_task=None, u_factor=None, output_filename = None):
        self.u_factor = u_factor
        self.nb_task = nb_task
        self.output_filename = output_filename
        self.tasks = [Task] * self.nb_task

    def generate_tasks(self):
        denominator = 100
        multiplier = 2  # 2 is the minimum to always have a random split of the utilization
        while self.nb_task > (self.u_factor * multiplier):
            multiplier += 1

        self.u_factor = self.u_factor * multiplier
        denominator = denominator * multiplier

        # Givin to each task a minimum utilization of 1, thus the utilization remaining is (utilization - nbTasks)
        utilisation_of_task = [1.0] * self.nb_task
        self.u_factor -= self.nb_task

        # giving randomly 1 more utilization unit until utilization reach 0 
        while self.u_factor > 0:
            ran_index = random.randint(0, len(utilisation_of_task) - 1)
            utilisation_of_task[ran_index] += 1
            self.u_factor -= 1

        # transforming utilization into it's real value, utilization/divider */
        for i in range(len(utilisation_of_task)):
            utilisation_of_task[i] = utilisation_of_task[i] / denominator

        max_period = 0
        for i in range(len(self.tasks)):
            # computing the value of period and computation of current task, considering that (Ci/Ti = Ui)
            period_of_task = 0
            computation_of_task = 0
            while computation_of_task == 0:
                period_of_task += 1
                computation_of_task = int(period_of_task * utilisation_of_task[i])

            # avoid loss of utilization (if there is one) until the last task of the system */
            if i < (len(utilisation_of_task) - 1):
                diff_util_and_real_util = utilisation_of_task[i] - (float(computation_of_task) / float(period_of_task))
                utilisation_of_task[i] -= diff_util_and_real_util
                utilisation_of_task[i + 1] += diff_util_and_real_util

            random_multiplier = random.randint(1, 2) * 5  # to make all value multiple of 5
            period_of_task *= random_multiplier
            computation_of_task *= random_multiplier

            # random value of the deadline on range [computation_of_task, period_of_task]
            deadline_of_task = computation_of_task + random.randint(0, ((period_of_task - computation_of_task) / 5)) * 5
            self.tasks[i] = Task(0, period_of_task, deadline_of_task, computation_of_task, i)
            # storing maximum value of the period for the offset value range 
            if period_of_task > max_period:
                max_period = period_of_task

        # setting the offset on range [0, max_period]
        for i in range(len(self.tasks)):
            self.tasks[i].offset = random.randint(0, (max_period / 5)) * 5
 
        self.output_to_file()

    def output_to_file(self):
        file = open(self.output_filename, "w")
        for i in range(len(self.tasks)):
            task_line = str(self.tasks[i].offset) + " " + str(self.tasks[i].period) + " " \
                       + str(self.tasks[i].deadline) + " " + str(self.tasks[i].computation)+"\n"
            file.write(task_line)
        file.close()
