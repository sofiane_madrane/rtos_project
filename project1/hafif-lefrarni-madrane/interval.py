from functools import reduce


def lcm(a, b):
    if a > b:
        greater = a
    else:
        greater = b

    while True:
        if greater % a == 0 and greater % b == 0:
            lcm = greater
            break
        greater += 1

    return lcm


def get_lcm_for(your_list):
    return reduce(lambda x, y: lcm(x, y), your_list)


def feasibility_interval(list_task):
    list_period = []
    list_release_time = []
    interval = []

    for task in list_task:
        list_period.append(task.period)
        list_release_time.append(task.offset)

    P = get_lcm_for(list_period)
    O_max = max(list_release_time)

    interval.append(O_max)
    interval.append((O_max+(2*P)))

    return interval
