Project 1 Audley’s priorities assignment algorithm

commands to run the project:

	> python project.py interval <tasksFile>
	> python project.py sim <start> <stop> <tasksFile>
	> python project.py audsley <start> <stop> <tasksFile>
	> python project.py gen <numberOfTask> <utilization> <output_filename>

Graphical library : matplotlib

Note : The visual output is produced and saved automatically as a pdf file when a simulation is done.
