import simulationPlotter


class Simulator:
    def __init__(self, task_set=None, start_interval=None, end_interval=None):
        if task_set is not None:
            self.produceOutput = True
        else:
            self.produceOutput = False
        self.task_system = task_set
        self.start_interval = start_interval
        self.end_interval = end_interval
        self.simulation_instant = 0
        self.simulation_history = []  # history of the scheduling given to the Scheduling plotter part
        self.job_set = []
        self.finished_job = []
        self.missed_job = []
        self.last_job_executed = None  # Last job that has been executed, if no job then value is None
        self.last_job_start = None  # simulation instant since when the last_job_executed is being executed
        self.step_output = [[], [], [], []]  # lists of output [executions, arrivals, deadlines, missed deadlines]

    # Reset all attribute used in a simulation to their default state
    def reset_simulation_attribute(self):
        self.simulation_instant = 0
        self.simulation_history = []
        self.job_set = []
        self.finished_job = []
        self.missed_job = []
        self.last_job_executed = None
        self.last_job_start = None

    # Simulate the system task in the interval asked
    def simulate(self):
        if self.end_interval is not None:
            self.reset_simulation_attribute()
            while self.simulation_instant < self.end_interval:
                self.release_jobs()
                self.execute_job()
                self.reached_jobs_deadline()
                self.simulation_instant += 1
                self.check_missed_deadlines()
                if self.simulation_instant == self.end_interval:
                    self.reached_jobs_deadline()
                self.print_sim_step_output()
            if self.produceOutput:
                simulationPlotter.plot_simulation(self.simulation_history)
                print("\nA visual output of the simulation has been saved as SchedulingVisualOutput.pdf")

    # Release jobs from task when their release time match the simulation instant
    # Add output about jobs released in the list of output
    def release_jobs(self):
        for task in self.task_system:
            if task.next_job_release == self.simulation_instant:
                job = task.release_a_job()
                self.add_output('a', job)
                self.job_set.append(job)
                task.update_release_time()
        self.job_set.sort(key=lambda x: x.get_priority(), reverse=False)

    # Execute the most prior job (if there is one) in the simulation
    # Add output about the execution time of a job int the list of output
    def execute_job(self):
        if len(self.job_set) == 0:
            self.simulation_history.append(-1)
            if self.last_job_executed is not None:  # if the last job executed in the job set has finish
                self.add_output('e', self.last_job_executed)
                self.last_job_executed = None
        else:
            job = self.job_set[0]
            if self.last_job_executed != job:  # if the next job executed != the last job executed
                if self.simulation_instant != 0:
                    if self.simulation_history[self.simulation_instant-1] != -1:
                        self.add_output('e', self.last_job_executed)
                self.last_job_start = self.simulation_instant
                self.last_job_executed = job
            self.simulation_history.append(job.get_task_identifier())
            job.increase_consumed_computation()
            if job.get_computation() == job.get_consumed_computation():
                self.finished_job.append(job)
                self.job_set.pop(0)

    # Add output about jobs that have reached their deadlines
    def reached_jobs_deadline(self):
        for job in self.finished_job:
            if job.get_absolute_deadline() == self.simulation_instant:
                self.add_output('d', job)

        for job in self.missed_job:
            if job.get_absolute_deadline() == self.simulation_instant:
                self.add_output('d', job)

    # Checks if any job has missed his deadline
    # Add output about jobs that have missed their deadlines
    def check_missed_deadlines(self):
        for job in self.job_set:
            if self.simulation_instant == job.get_absolute_deadline() \
                    and job.get_computation() > job.get_consumed_computation():
                self.add_output('m', job)
                self.missed_job.append(job)

    # Print the list of output for a simulation step
    def print_sim_step_output(self):
        # sort what's going to be printed so that the output match the order of the project example
        self.step_output[0].sort()
        self.step_output[1].sort()
        self.step_output[2].sort()
        self.step_output[3].sort()
        for step in self.step_output:
            for line in step:
                print(line)
            del step[:]  # delete what's has been printed to clear for the next step

    # Method called by other method to add an output to the list of output
    def add_output(self, mode, job):
        if self.produceOutput:
            if mode == 'a':  # arrival new job
                self.step_output[1].append("{0}: Arrival of job T{1}J{2}".format(self.simulation_instant,
                                                                                 job.get_task_identifier(),
                                                                                 job.get_instance_number()))
            elif mode == 'm':  # deadline miss
                self.step_output[3].append("{0}: Job T{1}J{2} misses a deadline".format(self.simulation_instant,
                                                                                        job.get_task_identifier(),
                                                                                        job.get_instance_number()))
            elif mode == 'e':  # job execution
                self.step_output[0].append("{0}-{1}: T{2}J{3}".format(self.last_job_start, self.simulation_instant,
                                                                      job.get_task_identifier(),
                                                                      job.get_instance_number()))
            elif mode == 'd':  # deadline reached
                self.step_output[2].append("{0}: Deadline of job T{1}J{2}".format(self.simulation_instant,
                                                                                  job.get_task_identifier(),
                                                                                  job.get_instance_number()))

    # Check if task is the lowest priority viable in a task system in the given interval
    # Return True if it is, False if not
    def is_lowest_priority_viable(self, task_sys, start_interval, end_interval, task_number):
        lowest_priority = len(task_sys) + 1
        for i in range(len(task_sys)):
            if task_sys[i].identifier == task_number:
                task_sys[i].priority = lowest_priority
            else:
                task_sys[i].priority = i
        self.task_system = task_sys
        self.start_interval = start_interval
        self.end_interval = end_interval
        self.simulate()
        for job in self.missed_job:
            if job.get_task_identifier() == task_number:
                return False
        return True
