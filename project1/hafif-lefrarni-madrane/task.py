from job import *


class Task:
    def __init__(self, offset, period, deadline, computation, identifier):
        self.offset = offset
        self.period = period
        self.deadline = deadline
        self.computation = computation
        self.identifier = identifier
        self.priority = identifier
        self.next_job_release = offset
        self.number_next_job = 0

    def update_release_time(self):
        self.next_job_release += self.period

    def release_a_job(self):
        self.number_next_job += 1
        job = Job(self.number_next_job, self.identifier, self.computation, (self.next_job_release+self.deadline),
                  self.next_job_release, self.priority)
        return job

    def __repr__(self):
        return ("Task %d : offset = %d, period = %d, deadline = %d, computation = %d" % (self.identifier, self.offset,
                                                                                       self.period, self.deadline,
                                                                                       self.computation))


# Parse file of tasks to set of tasks
def parser(task_file):
    task_set = []
    task_index = 1
    file = open(task_file, "r")
    for line in file.readlines():
        task_param = line.split()
        task = Task(int(task_param[0]), int(task_param[1]), int(task_param[2]), int(task_param[3]), task_index)
        task_set.append(task)
        task_index += 1
    return task_set
