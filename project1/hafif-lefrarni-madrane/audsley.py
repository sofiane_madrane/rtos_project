from simulator import *
import copy

# Recursive Audsley algorithm using 'is_lowest_priority_viable()' method
# of Simulator class to search all possibles priority assignment
def audsley_search(task_set, start_interval, end_interval, call=0):
    simulator = Simulator()
    for a_task in task_set:
        task_set_this = copy.deepcopy(task_set)
        a_task_is_lpv = simulator.is_lowest_priority_viable(task_set_this, start_interval, end_interval,
                                                            a_task.identifier)
        if a_task_is_lpv and len(task_set) > 1:
            print("%sTask %d is lowest priority viable" % ("\t"*call,a_task.identifier))
            task_set_next = copy.deepcopy(task_set)
            task_set_next = [task for task in task_set_next if task.identifier != a_task.identifier]
            audsley_search(task_set_next, start_interval, end_interval, call+1)
        else:
            print("%sTask %d is not lowest priority viable" % ("\t"*call, a_task.identifier))
