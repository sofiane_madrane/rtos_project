from audsley import *
from interval import *
from generator import *
import sys


def output_project_param_info():
    print("(INFO) Wrong number or name of parameters, these are the accepted commands :")
    print("\t- python project.py interval <tasksFile>")
    print("\t- python project.py sim <start> <stop> <tasksFile>")
    print("\t- python project.py audsley <start> <stop> <tasksFile>")
    print("\t- python project.py gen <numberOfTask> <utilization> <output_filename>")


if __name__ == "__main__":
        nb_argument = len(sys.argv)
        try:
            if nb_argument == 3:
                if sys.argv[1] == "interval":
                    task_system = parser(sys.argv[2])
                    print(feasibility_interval(task_system))
            elif nb_argument == 5:
                if sys.argv[1] == "sim":
                    start_interval = int(sys.argv[2])
                    end_interval = int(sys.argv[3])
                    task_system = parser(sys.argv[4])
                    simulator = Simulator(task_system, start_interval, end_interval)
                    simulator.simulate()
                elif sys.argv[1] == "gen":
                    nb_task = int(sys.argv[2])
                    utilization = int(sys.argv[3])
                    output_file_name = sys.argv[4]
                    gen = Generator(nb_task, utilization, output_file_name)
                    gen.generate_tasks()
                elif sys.argv[1] == "audsley":
                    start_interval = int(sys.argv[2])
                    end_interval = int(sys.argv[3])
                    task_system = parser(sys.argv[4])
                    audsley_search(task_system, start_interval, end_interval)
                else:
                    output_project_param_info()
            else:
                output_project_param_info()
        except FileNotFoundError:
            print("(Error) The file name given as a parameter doesn't exist")
